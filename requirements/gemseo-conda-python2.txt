# These dependencies must be compatible with the ones in setup.cfg/options.extras_require
pyside2 <=5.15.2
# provides both graphviz and dot
python-graphviz <=0.16
nlopt >=2.4.2,<=2.7.0
