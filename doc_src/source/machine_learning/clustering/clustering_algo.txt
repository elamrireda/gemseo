..
   Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com

   This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
   Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. uml::

	abstract class MLClusteringAlgo {
	 +n_clusters
	 +labels
	 +learn()
	 +predict()
	 +predict_proba()
	 #predict_proba()
	 #predict_proba_hard()
	 #predict_proba_soft()
	}

	class KMeans {
	 +ABBR
 	 +LIBRARY
	 +algo
	 #fit()
	 #predict()
	 #predict_proba_soft()
	}

	class GaussianMixture {
	 +ABBR
 	 +LIBRARY
	 +algo
	 #fit()
	 #predict()
	 #predict_proba_soft()
	}

	MLClusteringAlgo <|-- KMeans
	MLClusteringAlgo <|-- GaussianMixture
